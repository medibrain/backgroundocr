﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BackgroundOCR
{
    /// <summary>
    /// 2020年2月7日現在未使用
    /// </summary>
    public partial class FormManualReg : Form
    {
        Insurer insurer;

        public FormManualReg()
        {
            InitializeComponent();
            
            Insurer.GetInsurer();

            //リストボックスに表示
            listBox1.DataSource = Insurer.InsurerList;
            listBox1.DisplayMember = "InsurerName";
        }

        private void buttonNext_Click(object sender, EventArgs e)
        {
            //現在のデータベースを保管
            var currentDBName = DB.GetDBName();

            //DB指定
            DB.SetDBName(insurer.dbName);

            using (var f = new OpenFileDialog())
            {
                if (f.ShowDialog() != DialogResult.OK) return;

                //csvファイルの読込
                var lines = new List<string>();
                using (var sr = new System.IO.StreamReader(f.FileName + "\\OCR_Result.csv", Encoding.GetEncoding("Shift_JIS")))
                {
                    while (sr.Peek() > 0) lines.Add(sr.ReadLine());
                }

                for (int i = 0; i < lines.Count; i++)
                {
                    if (!Scan.SetOcrData(lines[i]))
                    {
                        Log.ErrorWrite(
                            $"手動指定  {i + 1}行目のOCR結果CSVによるApplicationレコード更新に失敗しました。");
                    }
                }
            }

            //接続先データベースを元に戻す
            DB.SetDBName(currentDBName);

            return;
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //対象Insurerインスタンスを指定
            insurer = (Insurer)listBox1.SelectedItem;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
