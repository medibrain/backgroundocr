﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NpgsqlTypes;

namespace BackgroundOCR
{
    public enum groupStatus { OCR待ち = 0, OCR中 = 1, OCR済み = 2, 入力中 = 3, 入力済み = 4, 
        ベリファイ済み = 5, 自動点検済み = 6, 点検作業中 = 7, 作業完了 = 8 };
    
    public class ScanGroup
    {
        public int GroupID { get; set; }
        public groupStatus Status { get; set; }
        public int ScanID { get; set; }
        public DateTime ScanDate { get; set; }
        public int ScanUser { get; set; }
        public DateTime CheckDate { get; set; }
        public int CheckUser { get; set; }
        public DateTime InquiryDate { get; set; }
        public int InquiryUser { get; set; }
        
        public int cyear { get; set; }
        public int cmonth { get; set; }
        public string note1 { get; set; }
        public string note2 { get; set; }

        public int nocheck { get; set; }
        public int yescheck { get; set; }
        public string userName { get; set; }
        
        public static bool DoGroup(Scan s, int gid)
        {
            var g = new ScanGroup();
            g.GroupID = gid;
            g.Status = groupStatus.OCR待ち;//todo:このへんの扱い
            g.ScanID = s.sid;
            g.ScanDate = s.scanDate;

            if (!g.Insert())
            {
                Log.ErrorWrite("Group IDの取得に失敗しました");
                return false;
            }

            return true;
        }
        
        /// <summary>
        /// groupテーブルにgroupIDを登録
        /// </summary>
        /// <returns></returns>
        private bool Insert()
        {
            using (var cmd = DB.CreateCmd("INSERT INTO scangroup " +
                "(groupid, status, scanid, scandate, scanuser, checkdate, checkuser, inquirydate, inquiryuser)" +
                "VALUES(:gid, :gst, :sid, :sd, :su, :cd, :cu, :qd, :qu) "))
            {
                cmd.Parameters.Add("gid", NpgsqlDbType.Integer).Value = (int)this.GroupID;
                cmd.Parameters.Add("gst", NpgsqlDbType.Integer).Value = (int)this.Status;
                cmd.Parameters.Add("sid", NpgsqlDbType.Integer).Value = (int)this.ScanID;
                cmd.Parameters.Add("sd", NpgsqlDbType.Date).Value = this.ScanDate;
                cmd.Parameters.Add("su", NpgsqlDbType.Integer).Value = this.ScanUser;
                cmd.Parameters.Add("cd", NpgsqlDbType.Date).Value = this.CheckDate;
                cmd.Parameters.Add("cu", NpgsqlDbType.Integer).Value = this.CheckUser;
                cmd.Parameters.Add("qd", NpgsqlDbType.Date).Value = this.InquiryDate;
                cmd.Parameters.Add("qu", NpgsqlDbType.Integer).Value = this.InquiryUser;

                try
                {
                    return cmd.TryExecuteNonQuery();
                }
                catch
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// GroupIDで単一のグループを取得
        /// </summary>
        /// <param name="gid"></param>
        /// <returns></returns>
        public static ScanGroup Select(int gid)
        {
            using (var cmd = DB.CreateCmd("SELECT " +
                "status, scanid, scandate, scanuser, checkdate, checkuser, inquirydate, inquiryuser " +
                "FROM scangroup WHERE groupid =:gid"))
            {
                cmd.Parameters.Add("gid", NpgsqlDbType.Integer).Value = gid;
                var res = cmd.TryExecuteReaderList();
                if (res == null || res.Count == 0) return null;

                var g = new ScanGroup();
                var item = res[0];
                g.Status = (groupStatus)(int)item[0];
                g.ScanID = (int)item[1];
                //g.scanDate = (DateTime)item[2];
                //g.scanUser = (int)item[3];
                //g.checkDate = (DateTime)item[4];
                //g.checkUser = (int)item[5];
                //g.inquiryDate = (DateTime)item[6];
                //g.inquiryUser = (int)item[7];
                g.ScanDate = item[3] != System.DBNull.Value ? (DateTime)item[3] : DateTime.Parse("2000/1/1");
                g.ScanUser = item[4] != System.DBNull.Value ? (int)item[4] : 0;
                g.CheckDate = item[5] != System.DBNull.Value ? (DateTime)item[5] : DateTime.Parse("2000/1/1");
                g.CheckUser = item[6] != System.DBNull.Value ? (int)item[6] : 0;
                g.InquiryDate = item[7] != System.DBNull.Value ? (DateTime)item[7] : DateTime.Parse("2000/1/1");
                g.InquiryUser = item[8] != System.DBNull.Value ? (int)item[8] : 0;

                return g;
            }
        }

        public bool Update()
        {
            using (var cmd = DB.CreateCmd("UPDATE scangroup SET " +
                "status=:gstatus, scanid=:sid, scandate=:sd, scanuser=:su, " +
                "checkdate=:cd, checkuser=:cu, inquirydate=:qd, inquiryuser=:qu " +
                "WHERE groupid=:gid"))
            {
                cmd.Parameters.Add("gstatus", NpgsqlDbType.Integer).Value = (int)this.Status;
                cmd.Parameters.Add("sid", NpgsqlDbType.Integer).Value = (int)this.ScanID;
                cmd.Parameters.Add("sd", NpgsqlDbType.Date).Value = this.ScanDate;
                cmd.Parameters.Add("su", NpgsqlDbType.Integer).Value = this.ScanUser;
                cmd.Parameters.Add("cd", NpgsqlDbType.Date).Value = this.CheckDate;
                cmd.Parameters.Add("cu", NpgsqlDbType.Integer).Value = this.CheckUser;
                cmd.Parameters.Add("qd", NpgsqlDbType.Date).Value = this.InquiryDate;
                cmd.Parameters.Add("qu", NpgsqlDbType.Integer).Value = this.InquiryUser;
                cmd.Parameters.Add("gid", NpgsqlDbType.Integer).Value = this.GroupID;

                try
                {
                    return cmd.TryExecuteNonQuery();
                }
                catch
                {
                    return false;
                }
            }
        }

        public static bool Update(ScanGroup sg)
        {
            using (var cmd = DB.CreateCmd("UPDATE scangroup SET " +
                "status=:gstatus, scanid=:sid, scandate=:sd, scanuser=:su, " +
                "checkdate=:cd, checkuser=:cu, inquirydate=:qd, inquiryuser=:qu " +
                "WHERE groupid=:gid"))
            {
                cmd.Parameters.Add("gstatus", NpgsqlDbType.Integer).Value = (int)sg.Status;
                cmd.Parameters.Add("sid", NpgsqlDbType.Integer).Value = (int)sg.ScanID;
                cmd.Parameters.Add("sd", NpgsqlDbType.Date).Value = sg.ScanDate;
                cmd.Parameters.Add("su", NpgsqlDbType.Integer).Value = sg.ScanUser;
                cmd.Parameters.Add("cd", NpgsqlDbType.Date).Value = sg.CheckDate;
                cmd.Parameters.Add("cu", NpgsqlDbType.Integer).Value = sg.CheckUser;
                cmd.Parameters.Add("qd", NpgsqlDbType.Date).Value = sg.InquiryDate;
                cmd.Parameters.Add("qu", NpgsqlDbType.Integer).Value = sg.InquiryUser;
                cmd.Parameters.Add("gid", NpgsqlDbType.Integer).Value = sg.GroupID;

                try
                {
                    return cmd.TryExecuteNonQuery();
                }
                catch(Exception ex)
                {
                    Log.ErrorWrite(ex);
                    return false;
                }
            }
        }

        /// <summary>
        /// 全てのグループを取得
        /// </summary>
        /// <returns></returns>
        public static List<ScanGroup> GetGroupList()
        {
            using (var cmd = DB.CreateCmd("SELECT " +
                "groupid, status, scanid, scandate, canuser, checkdate, checkuser, inquirydate, inquiryuser " +
                "FROM scangroup"))
            {
                var res = cmd.TryExecuteReaderList();
                if (res == null) return null;

                var l = new List<ScanGroup>();

                foreach (var item in res)
                {
                    var g = new ScanGroup();

                    g.GroupID = (int)item[0];
                    g.Status = (groupStatus)(int)item[1];
                    g.ScanID = (int)item[2];
                    g.ScanDate = item[3] != System.DBNull.Value ? (DateTime)item[3] : DateTime.Parse("2000/1/1");
                    g.ScanUser = item[4] != System.DBNull.Value ? (int)item[4] : 0;
                    g.CheckDate = item[5] != System.DBNull.Value ? (DateTime)item[5] : DateTime.Parse("2000/1/1");
                    g.CheckUser = item[6] != System.DBNull.Value ? (int)item[6] : 0;
                    g.InquiryDate = item[7] != System.DBNull.Value ? (DateTime)item[7] : DateTime.Parse("2000/1/1");
                    g.InquiryUser = item[8] != System.DBNull.Value ? (int)item[8] : 0;

                    l.Add(g);
                }

                return l;
            }
        }

        /// <summary>
        /// ステータスで複数のグループを取得
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        public static List<ScanGroup> GetScanGroupListST(groupStatus gstatus)
        {
            using (var cmd = DB.CreateCmd("SELECT " +
                "groupid, status, scanid, scandate, scanuser, checkdate, checkuser, inquirydate, inquiryuser " +
                "FROM scangroup WHERE status=:gst ORDER BY groupid"))
            {
                cmd.Parameters.Add("gst", NpgsqlDbType.Integer).Value = (int)gstatus;

                var res = cmd.TryExecuteReaderList();
                if (res == null) return null;

                var l = new List<ScanGroup>();

                foreach (var item in res)
                {
                    var g = new ScanGroup();

                    g.GroupID = (int)item[0];
                    g.Status = (groupStatus)(int)item[1];
                    g.ScanID = (int)item[2];
                    g.ScanDate = item[3] != System.DBNull.Value ? (DateTime)item[3] : DateTime.Parse("2000/1/1");
                    g.ScanUser = item[4] != System.DBNull.Value ? (int)item[4] : 0;
                    g.CheckDate = item[5] != System.DBNull.Value ? (DateTime)item[5] : DateTime.Parse("2000/1/1");
                    g.CheckUser = item[6] != System.DBNull.Value ? (int)item[6] : 0;
                    g.InquiryDate = item[7] != System.DBNull.Value ? (DateTime)item[7] : DateTime.Parse("2000/1/1");
                    g.InquiryUser = item[8] != System.DBNull.Value ? (int)item[8] : 0;

                    l.Add(g);
                }

                return l;
            }
        }

        /// <summary>
        /// ScanIDで複数のグループを取得
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        public static List<ScanGroup> GetScanGroupListSID(int sid)
        {
            var sql = "SELECT " +
                $"groupid, status, scanid, scandate, scanuser, checkdate, " +
                $"checkuser, inquirydate, inquiryuser " +
                $"FROM scangroup WHERE scanid={sid} ORDER BY groupid";

            return DB.Query<ScanGroup>(sql).ToList();
        }
    }
}
