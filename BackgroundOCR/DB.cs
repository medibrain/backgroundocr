﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Npgsql;
using Dapper;

namespace BackgroundOCR
{
    class DB
    {
        static string dbAddress = string.Empty;
        static string portNo = string.Empty;
        static string dbName = string.Empty;
        static string dbEncording = string.Empty;
        static string userName = string.Empty;
        static string password = string.Empty;
        static string connectionStr = string.Empty;
        
        //20200207134839 furukawa st ////////////////////////
        //保険者別接続先機能のためDBサーバ変数追加        
        static string dbserver = string.Empty;
        //20200207134839 furukawa ed ////////////////////////

        //20200207142119 furukawa st ////////////////////////

        /// <summary>
        /// 保険者別接続先
        /// </summary>
        static DataTable dtdb = new DataTable();
        //20200207142119 furukawa ed ////////////////////////


        //20200207153315 furukawa st ////////////////////////
        //settings.xml保持        
       // static DataSet dsSetting = new DataSet();
        //20200207153315 furukawa ed ////////////////////////


        static DB()
        {
            SetDBName("jyusei");

            //20200207144807 furukawa st ////////////////////////
            //保険者別接続先をロードする
            if (!LoadDB()) return;
            //20200207144807 furukawa ed ////////////////////////
        }


        //20200207141225 furukawa ////////////////////////
        /// <summary>
        /// Jyuseiから保険者別接続先を取得しておく
        /// </summary>
        /// <returns></returns>
        private static bool LoadDB()
        {
            if (dtdb.Rows.Count > 0) return true;

            Npgsql.NpgsqlConnection cn = new NpgsqlConnection();
            Npgsql.NpgsqlCommand cmd = new NpgsqlCommand();
            Npgsql.NpgsqlDataAdapter da = new NpgsqlDataAdapter();
            

            try
            {
                cn.ConnectionString = connectionStr;
                cn.Open();
                cmd.CommandText = "select * from insurer where enabled=true order by insurerid";
                cmd.Connection = cn;
                da.SelectCommand = cmd;
                dtdb.TableName = "dtdb";
                da.Fill(dtdb);


                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message,"Background OCR");
                return false;
            }
            finally
            {
                da.Dispose();
                cmd.Dispose();
                cn.Close();
            }
        }

    

        /// <summary>
        /// 設定をロードします
        /// </summary>
        /// <returns></returns>
        public static bool SetDBName(string dbname)
        {
            try
            {
                //20200207144849 furukawa st ////////////////////////
                //Jyuseiの場合はSetting.xmlから取得。それ以外はJyusei.Insurerを参照
                
                if (dbname == "jyusei")
                {
                    
                   // dsSetting.ReadXml("settings.xml");
                    //dbAddress = dsSetting.Tables[0].Rows[0]["ServerIP"].ToString();
                    dbAddress = Settings.strDBserver;

                    //20200210093017 furukawa st ////////////////////////
                    //試験環境用機能としてポート持たせる
                    
                    portNo = Settings.intPort.ToString();
                    //portNo = "5432";
                    //20200210093017 furukawa ed ////////////////////////

                    dbName = dbname;
                    
                }
                else
                {
                    //jyuseiDBはメホールAWSのdbを直接見るが、各保険者DBはOCR用にローカルのを見る

                    DataRow[] dr = dtdb.Select($"dbname='{dbname}'");
                    if (dr.Length > 1) return false;

                    //20210113141956 furukawa st ////////////////////////
                    //OCR用ローカルDBを更新するため、接続先はLocalhost
                    
                    dbAddress = "localhost";
                    portNo = "5440";
                                //dbAddress = dr[0]["dbserver"].ToString();
                                //portNo = dr[0]["dbport"].ToString();

                    //20210113141956 furukawa ed ////////////////////////


                    dbName = dr[0]["dbname"].ToString();
                }

                //dbAddress = BackgroundOCR.Properties.Settings.Default.ServerIP;//"localhost";
                //portNo = "5432";
                //dbName = dbname;

                //20200207144849 furukawa ed ////////////////////////


                //20200914furukawa
                if (dbAddress.Contains("aws"))
                {
                    dbEncording = "UTF8";
                    userName = "postgres";
                    password = "medibrain1128";

                    connectionStr = string.Format("Server={0};Port={1};Database={2};User Id={3};Password={4};SSLmode=require;timeout=30;connectionlifetime=30",
                        dbAddress,
                        portNo,
                        dbName,
                        userName,
                        password);
                    //connectionStr = string.Format("Server={0};Port={1};Database={2};User Id={3};Password={4};SSL=true;",
                    //    dbAddress,
                    //    portNo,
                    //    dbName,
                    //    userName,
                    //    password);
                }
                else
                {
                    dbEncording = "UTF8";
                    userName = "postgres";
                    password = "pass";

                    connectionStr = string.Format("Server={0};Port={1};Database={2};User Id={3};Password={4};",
                        dbAddress,
                        portNo,
                        dbName,
                        userName,
                        password);
                }

            }
            catch
            {
                return false;
            }
            return true;
        }

        public static string GetDBName()
        {
            return dbName;
        }


        //20200217135255 furukawa st ////////////////////////
        //表示用接続先文字列
        
        public static string GetConnectionInfo()
        {
            return $"DBServerIP={dbAddress};port={portNo};";
        }
        //20200217135255 furukawa ed ////////////////////////


        public static string GetConnectionString()
        {
            return connectionStr;
        }

        /// <summary>
        /// トランザクション
        /// </summary>
        public class Transaction : IDisposable
        {
            public NpgsqlConnection conn { get; private set; }
            public NpgsqlTransaction tran { get; private set; }

            public Transaction()
            {
                open();
                tran = conn.BeginTransaction();
            }

            private bool open()
            {
                conn = new NpgsqlConnection(connectionStr);
#if DEBUG
                conn.Open();
#else
                try
                {
                    conn.Open();
                }
                catch (Exception ex)
                {
                    //Log.ErrorWrite(ex);
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                    return false;
                }
                if (conn.FullState != System.Data.ConnectionState.Open) return false;
#endif
                return true;
            }

            private void Commit()
            {
                tran.Commit();
            }

            private void Rollback()
            {
                tran.Rollback();
            }

            public void Dispose()
            {
                conn.Close();
                conn.Dispose();
                tran.Dispose();
            }
        }

        /// <summary>
        /// トランザクションを開始します
        /// </summary>
        /// <returns></returns>
        public static Transaction CreateTransaction()
        {
            return new Transaction();
        }

        /// <summary>
        /// SQLコマンドを作成します。
        /// </summary>
        /// <param name="commandText"></param>
        /// <returns></returns>
        public static Command CreateCmd(string commandText, Transaction tran)
        {
            return new Command(commandText, tran);
        }

        /// <summary>
        /// SQLコマンドを作成します。
        /// </summary>
        /// <param name="commandText">コマンドtext</param>
        /// <returns></returns>
        public static Command CreateCmd(string commandText)
        {
            return new Command(commandText);
        }

        public class Command : IDisposable
        {
            NpgsqlConnection conn;
            NpgsqlCommand cmd;

            private bool open()
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
                conn = new NpgsqlConnection(connectionStr);
#if DEBUG
                //20201118180710 furukawa st ////////////////////////
                //接続失敗したらfalse
                
                try
                {
                    conn.Open();
                }
                catch(Exception ex)
                {
                    return false;
                }

                    //conn.Open();
                //20201118180710 furukawa ed ////////////////////////
#else
                try
                {
                    conn.Open();
                }
                catch (Exception ex)
                {
                    //Log.ErrorWrite(ex);
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                    return false;
                }
                if (conn.FullState != System.Data.ConnectionState.Open) return false;
#endif
                return true;
            }

            public NpgsqlParameterCollection Parameters
            {
                get { return cmd.Parameters; }
                set { value = cmd.Parameters; }
            }

            public Command(string commandText)
            {
                open();
                cmd = new NpgsqlCommand(commandText, conn);
            }

            public Command(string commandText, Transaction tran)
            {
                open();
                cmd = new NpgsqlCommand(commandText, tran.conn, tran.tran);
            }

            private bool reOpen()
            {
                if (!open()) return false;
                cmd.Connection = conn;
                return true;
            }

            /// <summary>
            /// トランザクションを使用しているかをチェックし、使用中ならプログラムを強制終了します。
            /// </summary>
            private void tranCheck()
            {
                if (cmd.Transaction != null)
                {
                    System.Environment.Exit(0);
                }
            }

            public int Timeout
            {
                get { return cmd.CommandTimeout; }
                set { cmd.CommandTimeout = value; }
            }

            private void systemExit()
            {
                System.Environment.Exit(0);
            }

            /// <summary>
            /// このコマンドによって使用されているすべてのリソースを解放します。
            /// </summary>
            public void Dispose()
            {
                if (cmd.Transaction == null)
                {
                    conn.Close();
                    conn.Dispose();
                }
                cmd.Dispose();
            }

            /// <summary>
            /// Tryを含んだExcuteReaderを発行し、リストで返します。失敗した場合 null が返ります。
            /// </summary>
            /// <param name="cmd"></param>
            /// <returns></returns>
            public List<object[]> TryExecuteReaderList()
            {
                var lst = new List<object[]>();

#if DEBUG
                using (var dr = cmd.ExecuteReader())
                {
                    var c = dr.FieldCount;
                    while (dr.Read())
                    {
                        var ojs = new object[c];
                        dr.GetValues(ojs);
                        lst.Add(ojs);
                    }
                }
                return lst;
#else
                try
                {
                    using (var dr = cmd.ExecuteReader())
                    {
                        var c = dr.FieldCount;
                        while (dr.Read())
                        {
                            var ojs = new object[c];
                            dr.GetValues(ojs);
                            lst.Add(ojs);
                        }
                    }
                    return lst;
                }
                catch (Exception ex)
                {
                    if (cmd.Transaction != null) throw ex;
                    if (!reOpen()) throw ex;
                }

                try
                {
                    using (var dr = cmd.ExecuteReader())
                    {
                        var c = dr.FieldCount;
                        while (dr.Read())
                        {
                            var ojs = new object[c];
                            dr.GetValues(ojs);
                            lst.Add(ojs);
                        }
                    }
                    return lst;
                }
                catch (Exception ex)
                {
                    var sfm = new System.Diagnostics.StackFrame(1).GetMethod();
                    var data = "Class:" + sfm.DeclaringType.FullName + " Method:" + sfm.Name;
                    ex.Data.Add("MethodName", data);
                    throw ex;
                }
#endif
            }

            /// <summary>
            /// Tryを含んだExecuteScalarを発行します。失敗した場合 DBNull.Value が返ります。
            /// </summary>
            /// <param name="cmd"></param>
            /// <returns></returns>
            public object TryExecuteScalar()
            {
#if DEBUG
                return cmd.ExecuteScalar();
#else
                try
                {
                    return cmd.ExecuteScalar();
                }
                catch (Exception ex)
                {
                    if (cmd.Transaction != null) throw ex;
                    if (!reOpen()) throw ex;
                }

                try
                {
                    return cmd.ExecuteScalar();
                }
                catch (Exception ex)
                {
                    var sfm = new System.Diagnostics.StackFrame(1).GetMethod();
                    var data = "Class:" + sfm.DeclaringType.FullName + " Method:" + sfm.Name;
                    ex.Data.Add("MethodName", data);
                    throw ex;
                }
#endif
            }

            /// <summary>
            /// Tryを含んだExecuteNonQueryを発行します。失敗した場合 false が返ります。
            /// </summary>
            /// <param name="cmd"></param>
            /// <returns></returns>
            public bool TryExecuteNonQuery()
            {
#if DEBUG
                cmd.ExecuteNonQuery();
                return true;
#else
                try
                {
                    cmd.ExecuteNonQuery();
                    return true;
                }
                catch (Exception ex)
                {
                    if (cmd.Transaction != null) throw ex;
                    if (!reOpen()) throw ex;
                }

                try
                {
                    cmd.ExecuteNonQuery();
                    return true;
                }
                catch (Exception ex)
                {
                    var sfm = new System.Diagnostics.StackFrame(1).GetMethod();
                    var data = "Class:" + sfm.DeclaringType.FullName + " Method:" + sfm.Name;
                    ex.Data.Add("MethodName", data);
                    throw ex;
                }
#endif
            }
        }
        /// <summary>
        /// Dapperによる自動マッピングQuery
        /// </summary>
        public static IEnumerable<Type> Query<Type>(string sql, object parameters =null, Transaction tran = null)
        {
            try
            {
                if (tran == null)
                {
                    var conn = new Npgsql.NpgsqlConnection(connectionStr);
                    return conn.Query<Type>(sql, parameters);
                }
                else
                {
                    return tran.conn.Query<Type>(sql, parameters, tran.tran);
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return null;
            }
        }

        /// <summary>
        /// Dapperによる自動マッピングExcute
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool Excute(string sql, object obj = null, Transaction tran = null)
        {
            try
            {
                if (tran == null)
                {
                    var conn = new Npgsql.NpgsqlConnection(connectionStr);
                    conn.Execute(sql, obj);
                }
                else
                {
                    tran.conn.Execute(sql, obj, tran.tran);
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            return true;
        }
    }
}