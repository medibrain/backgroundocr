﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NpgsqlTypes;

namespace BackgroundOCR
{
    public class Insurer
    {
        public int InsurerID { set; get; }
        public string InsurerName { set; get; }
        public string dbName { set; get; }
        public int ScanOrder { set; get; }

        //20200207112147 furukawa st ////////////////////////
        //保険者別接続先変数
        
        public string dbServer { get; set; }
        public int dbport { get; set; }
        public string imagefolderpath { get; set; }
        //20200207112147 furukawa ed ////////////////////////

        //20200917095303 furukawa st ////////////////////////
        //保険者ごとにdbUser、dbPass追加
        
        public string dbUser { get; set; }
        public string dbPass { get; set; }
        //20200917095303 furukawa ed ////////////////////////


        public static List<Insurer> InsurerList = new List<Insurer>();

        //20200917095322 furukawa st ////////////////////////
        //保険者ごとにdbUser、dbPass追加
        
        public Insurer(int iid, string iname, string dbname, int scanorder,
            string dbServer, int dbport, string imagefolderpath, string dbuser, string dbpass)

            //20200207112342 furukawa st ////////////////////////
            //保険者別接続先機能のため引数追加        
            //public Insurer(int iid, string iname, string dbname, int scanorder,string dbServer,int dbport,string imagefolderpath)
            //public Insurer(int iid, string iname, string dbname, int scanorder)
            //20200207112342 furukawa ed ////////////////////////
        //20200917095322 furukawa ed ////////////////////////
        {
            this.InsurerID = iid;
            this.InsurerName = iname;
            this.dbName = dbname;


            //20200207112226 furukawa st ////////////////////////
            //保険者別接続先を取得
            
            this.dbServer =dbServer ;
            this.dbport =dbport ;
            this.imagefolderpath =imagefolderpath;
            //20200207112226 furukawa ed ////////////////////////


            //20200917095348 furukawa st ////////////////////////
            //保険者ごとにdbUser、dbPass追加
            
            this.dbUser = dbuser;
            this.dbPass = dbpass;
            //20200917095348 furukawa ed ////////////////////////
        }

        
        //保険者一覧を取得
        public static void GetInsurer()
        {
            //現在のデータベース名を保存
            var currentDBName = DB.GetDBName();

            DB.SetDBName("jyusei");


            //20200917095024 furukawa st ////////////////////////
            //保険者ごとにdbUser、dbPass追加
            
            using (var cmd = DB.CreateCmd("SELECT " +
            "insurerid, insurername, dbname, scanorder ," +
            "dbserver,dbport,imagefolderpath,dbuser,dbpass " +
           "FROM insurer " +
           "WHERE enabled=true " +
           "ORDER BY scanorder"))

            #region old
            //20200207112553 furukawa st ////////////////////////
            //保険者別接続先機能のため取得フィールド追加

            // using (var cmd = DB.CreateCmd("SELECT " +
            // "insurerid, insurername, dbname, scanorder ," +
            // "dbserver,dbport,imagefolderpath " +
            //"FROM insurer " +
            //"WHERE enabled=true " +
            //"ORDER BY scanorder"))

            //20190822153332 furukawa st ////////////////////////
            //現在有効でない保険者は取らない

            //using (var cmd = DB.CreateCmd("SELECT " +
            //    "insurerid, insurername, dbname, scanorder " +
            //    "FROM insurer " +
            //    "WHERE enabled=true " +
            //    "ORDER BY scanorder"))

            // using (var cmd = DB.CreateCmd("SELECT " +
            //"insurerid, insurername, dbname, scanorder " +
            //"FROM insurer " +
            //"ORDER BY scanorder"))

            //20190822153332 furukawa ed ////////////////////////


            //20200207112553 furukawa ed ////////////////////////
            #endregion


            //20200917095024 furukawa ed ////////////////////////


            {
                var res = cmd.TryExecuteReaderList();
                if (res == null)
                {
                    //もし保険者情報が無ければアクセス先データベースを元に戻す
                    DB.SetDBName(currentDBName);
                    cmd.Dispose();
                    return;
                }

                InsurerList.Clear();

                foreach (var item in res)
                {
                    int iid = (int)item[0];
                    string iname = (string)item[1];
                    string dbname = (string)item[2];
                    int order = (int)item[3];


                    //ローカルdb専用なので固定値、画像フォルダのみ保険者別
                    string dbserver = "localhost";// (string)item[4];
                    int dbport = 5440;// (int)item[5];
                    string imagefolderpath = (string)item[6];

                    string dbuser = "postgres";// (string)item[7];
                    string dbpass = "pass";// (string)item[8];



                                                            ////20200207112846 furukawa st ////////////////////////
                                                            ////保険者別接続先機能のため変数追加

                                                            //string dbserver = (string)item[4];
                                                            //int dbport = (int)item[5];
                                                            //string imagefolderpath = (string)item[6];
                                                            ////20200207112846 furukawa ed ////////////////////////


                                                            ////20200917095617 furukawa st ////////////////////////
                                                            ////保険者ごとにdbUser、dbPass追加

                                                            //string dbuser = (string)item[7];
                                                            //string dbpass = (string)item[8];
                                                            ////20200917095617 furukawa ed ////////////////////////


                    //20200917095649 furukawa st ////////////////////////
                    //保険者ごとにdbUser、dbPass追加


                    var u = new Insurer(iid, iname, dbname, order, dbserver, dbport, imagefolderpath,dbuser,dbpass);




                            //20200207112929 furukawa st ////////////////////////
                            //保険者別接続先機能のため引数追加

                            //var u = new Insurer(iid, iname, dbname, order,dbserver,dbport,imagefolderpath);

                            //var u = new Insurer(iid, iname, dbname, order);
                            //20200207112929 furukawa ed ////////////////////////

                    //20200917095649 furukawa ed ////////////////////////

                    InsurerList.Add(u);
                }



                cmd.Dispose();
            }
            //アクセス先データベースを元に戻す
            DB.SetDBName(currentDBName);
        }
    }
}
