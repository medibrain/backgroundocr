﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BackgroundOCR
{
    public partial class FormMain : Form
    {
        bool ocring = false;

        //20200207162711 furukawa st ////////////////////////
        //Settings.xmlロード        
        Settings settings = new Settings();
        //20200207162711 furukawa ed ////////////////////////


        public FormMain()
        {
            InitializeComponent();

        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            //20200207162432 furukawa st ////////////////////////
            //監視タイマーインターバルもSettings.xmlから取得

            timer1.Interval = Settings.intTimerInterval;

            //タイマーインターバルの設定
            //timer1.Interval = 10000;
            //20200207162432 furukawa ed ////////////////////////



            notifyIcon1.Visible = true;

            //20200217135401 furukawa st ////////////////////////
            //接続先表示
            
            lblconn.Text = "jyusei:" + DB.GetConnectionInfo();
            //20200217135401 furukawa ed ////////////////////////

            //20201119085636 furukawa st ////////////////////////
            //バージョン番号
            
            System.Reflection.Assembly ass;
            ass = System.Reflection.Assembly.GetExecutingAssembly();
            System.Version ver = ass.GetName().Version;


            //20210113142549 furukawa st ////////////////////////
            //ローカルバージョン明示
            
            lblver.Text = $"Local_{ver.Major}.{ver.Minor}.{ver.Build}.{ver.MinorRevision.ToString().Substring(2)}";
            //lblver.Text = $"{ver.Major}.{ver.Minor}.{ver.Build}.{ver.MinorRevision.ToString().Substring(2)}";
            //20210113142549 furukawa ed ////////////////////////

            //20201119085636 furukawa ed ////////////////////////    

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (timer1.Enabled)
            {
                timer1.Stop();
                btnStart.Text = "開始";
                WriteLog("スキャンの監視を停止しました");
                textBoxInsurer.Text = "";
                textBoxScanID.Text = "";
                textBoxTime.Text = "";            }
            else
            {
                timer1.Start();
                btnStart.Text = "停止";
                WriteLog("スキャンの監視を開始しました");
                textBoxInsurer.Text = "Data searching...";
            }
        }

    
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (ocring) return;
            ocring = true;

            textBoxInsurer.Text = "last check at " + DateTime.Now.ToString("HH:mm:ss");

            //保険者一覧を取得
            Insurer.GetInsurer();

         

            Task.Factory.StartNew(() =>
                {
                    try
                    {
                        foreach (var item in Insurer.InsurerList)
                        {
                            //Scan単位でのOCR完了ごとにループを抜け、OCR待ちリストを更新する                            
                            if (Scan.DoOCR(item, this)) return;
                        }
                    }
                    finally
                    {
                        ocring = false;
                        
                    }
                });
        }

        DateTime ocrStartTime;

        public void TimeCountStart(Insurer ins, Scan scan)
        {
            ocrStartTime = DateTime.Now;
            this.Invoke(new Action(() =>
                {
                    textBoxInsurer.Text = ins.InsurerName;
                    textBoxScanID.Text = scan.sid.ToString();
                    textBoxTime.Text = DateTime.Now.ToString("HH:mm:ss");
                }));
        }

        public void TimeCountStop()
        {
            this.Invoke(new Action(() =>
                {
                    timerOcrCount.Stop();
                    textBoxTime.Text = DateTime.Now.ToString("HH:mm:ss");// "0:00:00";
                    textBoxInsurer.Text = "OCR finished.";
                }));
        }

        //表示管理
        //経過時間表示用別タイマー
        private void timerOcrCount_Tick(object sender, EventArgs e)
        {
            var span = DateTime.Now - ocrStartTime;
            textBoxTime.Text = span.ToString("HH:mm:ss");
        }


        //ストリップメニュー・終了処理
        //フォームからは終了できなくする
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Visible = false;
        }

        private void 表示ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Visible = true;
        }

        private void 終了ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var res=MessageBox.Show("本当に終了しますか？", "BackGroundOCR", MessageBoxButtons.YesNo,
                MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (res != System.Windows.Forms.DialogResult.Yes) return;

            if (timer1.Enabled) MessageBox.Show("このプログラムは終了しますが、\r\n現在実行中のOCRは最後まで実行されます。");
            
            notifyIcon1.Visible = false;
            
            Environment.Exit(0);
        }

        private void 設定ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (FormSetting f = new FormSetting())
            {
                f.ShowDialog();
            }
        }


        /// <summary>
        /// 2020年2月7日現在未使用
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 手動OCR結果登録ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (FormManualReg fmr = new FormManualReg())
            {
                fmr.ShowDialog();
            }
        }

        public void WriteLog(string log)
        {
            if (this.Visible)

            {
                this.Invoke(new Action(()=>
                    {


                        if (Settings.intOrder == 0) textBoxLog.Text += DateTime.Now.ToString("MM/dd HH:mm:ss") + "  " + log + "\r\n";
                        if (Settings.intOrder == 1) textBoxLog.Text = DateTime.Now.ToString("MM/dd HH:mm:ss") + "  " + log + "\r\n" + textBoxLog.Text;

                        //textBoxLog.Text += DateTime.Now.ToString("MM/dd HH:mm:ss") + "  " + log + "\r\n";
                    }));
            }
            else
            {
                if (Settings.intOrder == 0) textBoxLog.Text += DateTime.Now.ToString("MM/dd HH:mm:ss") + "  " + log + "\r\n";
                if (Settings.intOrder == 1) textBoxLog.Text = DateTime.Now.ToString("MM/dd HH:mm:ss") + "  " + log + "\r\n" + textBoxLog.Text;

                //textBoxLog.Text += DateTime.Now.ToString("MM/dd HH:mm:ss") + "  " + log + "\r\n";
            }

            //20220220145140 furukawa st ////////////////////////
            //ログが15000字を越えたら5000までに削る
            if (textBoxLog.TextLength > 15000) textBoxLog.Text = textBoxLog.Text.Substring(0, 5000);
            //20220220145140 furukawa ed ////////////////////////
        }

        private void btnInsSetting_Click(object sender, EventArgs e)
        {
            SettingForm_Insurer frm = new SettingForm_Insurer();
            frm.ShowDialog();
        }

    }
}
