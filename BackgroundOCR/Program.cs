﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Threading;

namespace BackgroundOCR
{
    static class Program
    {
        /// <summary>
        /// アプリケーションのメイン エントリ ポイントです。
        /// </summary>
        [STAThread]
        static void Main()
        {
            const string mutexName = "BackgroundOCR";
            bool hasHandle = false;

            using (var mutex = new Mutex(false, mutexName))
            {
                try
                {
                    try
                    {
                        hasHandle = mutex.WaitOne(0, false);
                    }
                    catch (System.Threading.AbandonedMutexException)
                    {
                        hasHandle = true;
                    }

                    if (hasHandle == false)
                    {
                        MessageBox.Show("BackGroundOCRは既に起動していると思われます。多重起動はできません。");
                        return;
                    }

                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);

#if DEBUG

#else
                    // ThreadExceptionイベント・ハンドラを登録する
                    Application.ThreadException += new ThreadExceptionEventHandler(Application_ThreadException);

                    // UnhandledExceptionイベント・ハンドラを登録する
                    Thread.GetDomain().UnhandledException += new UnhandledExceptionEventHandler(Application_UnhandledException);
#endif

                    Application.Run(new FormMain());
                }
                finally
                {
                    if (hasHandle) mutex.ReleaseMutex();
                }
            }
        }

        // 未処理例外をキャッチするイベント・ハンドラ
        // （Windowsアプリケーション用）
        public static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            try
            {
                Log.ErrorWrite(e.Exception);
            }
            finally
            {
                Environment.Exit(0);
            }
        }

        // 未処理例外をキャッチするイベント・ハンドラ
        // （主にコンソール・アプリケーション用）
        public static void Application_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception ex = e.ExceptionObject as Exception;
            try
            {
                if (ex != null) Log.ErrorWrite(ex);
            }
            finally
            {
                Environment.Exit(0);
            }
        }
    }
}
