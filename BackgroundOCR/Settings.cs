﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackgroundOCR
{
    //20200207162229 furukawa 
    /// <summary>
    /// settings.xmlロード処理専用クラス
    /// </summary>
    class Settings
    {
        /// <summary>
        /// OCRプログラムのパス
        /// </summary>
        public static string strOCRProgram = string.Empty;

        /// <summary>
        /// JyuseiDBのDBサーバIP
        /// </summary>
        public static string strDBserver = string.Empty;

        /// <summary>
        /// 画像フォルダパス
        /// </summary>
        public static string strImageFolder = string.Empty;

        /// <summary>
        /// 監視タイマーインターバル
        /// </summary>
        public static int intTimerInterval = 0;

        /// <summary>
        /// DBポート
        /// </summary>
        public static int intPort = 5432;

        /// <summary>
        /// Settings.xml保持
        /// </summary>
        static System.Data.DataSet dsSetting = new System.Data.DataSet();



        //20200217144724 furukawa st ////////////////////////
        //進行表示のソート順設定。0=通常(下が最新）1=上が最新

        /// <summary>
        /// ログのソート設定。0=通常(下が最新）1=上が最新
        /// </summary>
        public static int intOrder = 0;

        //20200217144724 furukawa ed ////////////////////////


        static Settings()
        {
            try
            {
                dsSetting.ReadXml("settings.xml");
                strOCRProgram = dsSetting.Tables[0].Rows[0]["OCRApp"].ToString();
                strDBserver = dsSetting.Tables[0].Rows[0]["ServerIP"].ToString();
                strImageFolder = dsSetting.Tables[0].Rows[0]["ImageFolder"].ToString();
                intTimerInterval = int.Parse(dsSetting.Tables[0].Rows[0]["TimerInterval"].ToString());
                intPort = int.Parse(dsSetting.Tables[0].Rows[0]["port"].ToString());
                
                //20200217144855 furukawa st ////////////////////////
                //進行表示のソート順設定
                
                intOrder = int.Parse(dsSetting.Tables[0].Rows[0]["sort"].ToString());
                //20200217144855 furukawa ed ////////////////////////
            }
            catch (System.IO.FileNotFoundException fnfEx)
            {
                throw new System.IO.FileNotFoundException("settings.xmlが見つかりません",fnfEx);
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }


        }


        /// <summary>
        /// settings.xml変更
        /// </summary>
        /// <param name="_strDBserver">jyuseiのdbserver</param>
        /// <param name="_strDBPort">jyuseiのポート番号</param>
        /// <param name="_strOCRApp">OCRアプリケーションの起動パス</param>
        /// <param name="_ImageFolder">画像スキャン後の置き場</param>
        /// <param name="_TimerInterval">OCR監視インターバル</param>
        /// <returns></returns>
        public static bool SettingChange(
            string _strDBserver,
            string _strDBPort,
            string _strOCRApp,
            string _ImageFolder,
            string _TimerInterval
            )
        {
            try
            {
                if (_TimerInterval == string.Empty) _TimerInterval = intTimerInterval.ToString();
                if (_ImageFolder == string.Empty) _ImageFolder = strImageFolder;
                if (_strDBserver == string.Empty) _strDBserver = strDBserver;
                if (_strDBPort == string.Empty) _strDBPort = intPort.ToString();
                if (_strOCRApp == string.Empty) _strOCRApp = strOCRProgram;

                dsSetting.Tables[0].Rows[0]["ServerIP"] = _strDBserver;
                dsSetting.Tables[0].Rows[0]["port"] = _strDBPort;
                dsSetting.Tables[0].Rows[0]["ImageFolder"] = _ImageFolder;
                dsSetting.Tables[0].Rows[0]["TimerInterval"] = _TimerInterval;
                dsSetting.Tables[0].Rows[0]["OCRApp"] = _strOCRApp;


                dsSetting.WriteXml("settings.xml");


                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
                return false;
            }
        }
    }
}
