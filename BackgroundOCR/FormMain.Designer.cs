﻿namespace BackgroundOCR
{
    partial class FormMain
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.設定ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.表示ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.終了ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnStart = new System.Windows.Forms.Button();
            this.labelSid = new System.Windows.Forms.Label();
            this.labelTime = new System.Windows.Forms.Label();
            this.labelInsurerName = new System.Windows.Forms.Label();
            this.timerOcrCount = new System.Windows.Forms.Timer(this.components);
            this.textBoxScanID = new System.Windows.Forms.TextBox();
            this.textBoxInsurer = new System.Windows.Forms.TextBox();
            this.textBoxTime = new System.Windows.Forms.TextBox();
            this.textBoxLog = new System.Windows.Forms.TextBox();
            this.lblconn = new System.Windows.Forms.Label();
            this.btnInsSetting = new System.Windows.Forms.Button();
            this.lblver = new System.Windows.Forms.Label();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.ContextMenuStrip = this.contextMenuStrip1;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "バックグラウンドOCR";
            this.notifyIcon1.Visible = true;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.設定ToolStripMenuItem,
            this.表示ToolStripMenuItem,
            this.toolStripSeparator1,
            this.終了ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(101, 76);
            // 
            // 設定ToolStripMenuItem
            // 
            this.設定ToolStripMenuItem.Name = "設定ToolStripMenuItem";
            this.設定ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.設定ToolStripMenuItem.Text = "設定";
            this.設定ToolStripMenuItem.Click += new System.EventHandler(this.設定ToolStripMenuItem_Click);
            // 
            // 表示ToolStripMenuItem
            // 
            this.表示ToolStripMenuItem.Name = "表示ToolStripMenuItem";
            this.表示ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.表示ToolStripMenuItem.Text = "表示";
            this.表示ToolStripMenuItem.Click += new System.EventHandler(this.表示ToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(97, 6);
            // 
            // 終了ToolStripMenuItem
            // 
            this.終了ToolStripMenuItem.Name = "終了ToolStripMenuItem";
            this.終了ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.終了ToolStripMenuItem.Text = "終了";
            this.終了ToolStripMenuItem.Click += new System.EventHandler(this.終了ToolStripMenuItem_Click);
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(617, 12);
            this.btnStart.Margin = new System.Windows.Forms.Padding(4);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(106, 47);
            this.btnStart.TabIndex = 0;
            this.btnStart.Text = "開始";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.button1_Click);
            // 
            // labelSid
            // 
            this.labelSid.AutoSize = true;
            this.labelSid.Location = new System.Drawing.Point(13, 12);
            this.labelSid.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelSid.Name = "labelSid";
            this.labelSid.Size = new System.Drawing.Size(52, 16);
            this.labelSid.TabIndex = 1;
            this.labelSid.Text = "ScanID";
            // 
            // labelTime
            // 
            this.labelTime.AutoSize = true;
            this.labelTime.Location = new System.Drawing.Point(505, 12);
            this.labelTime.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTime.Name = "labelTime";
            this.labelTime.Size = new System.Drawing.Size(68, 16);
            this.labelTime.TabIndex = 3;
            this.labelTime.Text = "開始時間";
            // 
            // labelInsurerName
            // 
            this.labelInsurerName.AutoSize = true;
            this.labelInsurerName.Location = new System.Drawing.Point(83, 12);
            this.labelInsurerName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelInsurerName.Name = "labelInsurerName";
            this.labelInsurerName.Size = new System.Drawing.Size(68, 16);
            this.labelInsurerName.TabIndex = 4;
            this.labelInsurerName.Text = "保険者名";
            // 
            // timerOcrCount
            // 
            this.timerOcrCount.Interval = 1000;
            this.timerOcrCount.Tick += new System.EventHandler(this.timerOcrCount_Tick);
            // 
            // textBoxScanID
            // 
            this.textBoxScanID.BackColor = System.Drawing.SystemColors.Control;
            this.textBoxScanID.Location = new System.Drawing.Point(8, 33);
            this.textBoxScanID.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxScanID.Name = "textBoxScanID";
            this.textBoxScanID.Size = new System.Drawing.Size(65, 22);
            this.textBoxScanID.TabIndex = 5;
            // 
            // textBoxInsurer
            // 
            this.textBoxInsurer.BackColor = System.Drawing.SystemColors.Control;
            this.textBoxInsurer.Location = new System.Drawing.Point(77, 33);
            this.textBoxInsurer.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxInsurer.Name = "textBoxInsurer";
            this.textBoxInsurer.Size = new System.Drawing.Size(418, 22);
            this.textBoxInsurer.TabIndex = 6;
            // 
            // textBoxTime
            // 
            this.textBoxTime.BackColor = System.Drawing.SystemColors.Control;
            this.textBoxTime.Location = new System.Drawing.Point(500, 33);
            this.textBoxTime.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxTime.Name = "textBoxTime";
            this.textBoxTime.Size = new System.Drawing.Size(92, 22);
            this.textBoxTime.TabIndex = 7;
            // 
            // textBoxLog
            // 
            this.textBoxLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxLog.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxLog.Location = new System.Drawing.Point(8, 67);
            this.textBoxLog.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxLog.Multiline = true;
            this.textBoxLog.Name = "textBoxLog";
            this.textBoxLog.ReadOnly = true;
            this.textBoxLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxLog.Size = new System.Drawing.Size(862, 430);
            this.textBoxLog.TabIndex = 8;
            // 
            // lblconn
            // 
            this.lblconn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblconn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblconn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblconn.Location = new System.Drawing.Point(8, 501);
            this.lblconn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblconn.Name = "lblconn";
            this.lblconn.Size = new System.Drawing.Size(863, 21);
            this.lblconn.TabIndex = 9;
            // 
            // btnInsSetting
            // 
            this.btnInsSetting.Location = new System.Drawing.Point(731, 12);
            this.btnInsSetting.Margin = new System.Windows.Forms.Padding(4);
            this.btnInsSetting.Name = "btnInsSetting";
            this.btnInsSetting.Size = new System.Drawing.Size(135, 47);
            this.btnInsSetting.TabIndex = 10;
            this.btnInsSetting.Text = "保険者設定";
            this.btnInsSetting.UseVisualStyleBackColor = true;
            this.btnInsSetting.Click += new System.EventHandler(this.btnInsSetting_Click);
            // 
            // lblver
            // 
            this.lblver.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblver.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblver.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblver.Location = new System.Drawing.Point(8, 522);
            this.lblver.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblver.Name = "lblver";
            this.lblver.Size = new System.Drawing.Size(863, 21);
            this.lblver.TabIndex = 9;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Honeydew;
            this.ClientSize = new System.Drawing.Size(879, 552);
            this.Controls.Add(this.btnInsSetting);
            this.Controls.Add(this.lblver);
            this.Controls.Add(this.lblconn);
            this.Controls.Add(this.textBoxLog);
            this.Controls.Add(this.textBoxTime);
            this.Controls.Add(this.textBoxInsurer);
            this.Controls.Add(this.textBoxScanID);
            this.Controls.Add(this.labelInsurerName);
            this.Controls.Add(this.labelTime);
            this.Controls.Add(this.labelSid);
            this.Controls.Add(this.btnStart);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximumSize = new System.Drawing.Size(1328, 1222);
            this.MinimumSize = new System.Drawing.Size(661, 255);
            this.Name = "FormMain";
            this.Text = "BackGroundOCR_LOCAL";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Shown += new System.EventHandler(this.Form1_Shown);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Label labelSid;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 表示ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem 終了ToolStripMenuItem;
        private System.Windows.Forms.Label labelTime;
        private System.Windows.Forms.Label labelInsurerName;
        private System.Windows.Forms.Timer timerOcrCount;
        private System.Windows.Forms.TextBox textBoxScanID;
        private System.Windows.Forms.TextBox textBoxInsurer;
        private System.Windows.Forms.TextBox textBoxTime;
        private System.Windows.Forms.ToolStripMenuItem 設定ToolStripMenuItem;
        private System.Windows.Forms.TextBox textBoxLog;
        private System.Windows.Forms.Label lblconn;
        private System.Windows.Forms.Button btnInsSetting;
        private System.Windows.Forms.Label lblver;
    }
}

