﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BackgroundOCR
{
    public partial class FormSetting : Form
    {
              
        /// <summary>
        /// setting.xmlロード
        /// </summary>
        public FormSetting()
        {
            InitializeComponent();

            //20200210115329 furukawa st ////////////////////////
            //設定をsetting.xmlに共通化した
            

            //textBoxServerIP.Text = BackgroundOCR.Properties.Settings.Default.ServerIP;
            //textBoxImageFolder.Text = BackgroundOCR.Properties.Settings.Default.ImageFolder;
            //textBoxOCR.Text = BackgroundOCR.Properties.Settings.Default.OCRApp;


            textBoxServerIP.Text = Settings.strDBserver;
            textBoxOCR.Text = Settings.strOCRProgram;
            textBoxPort.Text = Settings.intPort.ToString();
            textBoxImageFolder.Text = Settings.strImageFolder;
            textBoxInterval.Text = Settings.intTimerInterval.ToString() ;

            //20200210115329 furukawa ed ////////////////////////-


        }



        /// <summary>
        /// setting.xml更新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonOK_Click(object sender, EventArgs e)
        {

            //20200210120744 furukawa st ////////////////////////
            //settings.xmlに共通化したのでSettingx.xml更新
            
            try
            {
                //一応バックアップ
                System.IO.File.Copy("settings.xml", $"settings_bk_{DateTime.Now.ToString("yyyyMMdd-HHmmss")}.xml");

                Settings.SettingChange(
                     textBoxServerIP.Text,
                     textBoxPort.Text,
                     textBoxOCR.Text,
                     textBoxImageFolder.Text,
                     textBoxInterval.Text);

                

                MessageBox.Show("更新しました。BackgroundOCRを再起動してください",
                    Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                //落とす
                Environment.Exit(0);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }


                                //BackgroundOCR.Properties.Settings.Default.ServerIP = textBoxServerIP.Text;
                                //BackgroundOCR.Properties.Settings.Default.ImageFolder = textBoxImageFolder.Text;
                                //BackgroundOCR.Properties.Settings.Default.OCRApp = textBoxOCR.Text;
                                //Properties.Settings.Default.Save();

            //20200210120744 furukawa ed ////////////////////////


            this.Close();
            
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
