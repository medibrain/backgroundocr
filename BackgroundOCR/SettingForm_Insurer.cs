﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//20200131104223 furukawa 保険者別設定画面


namespace BackgroundOCR
{

    public partial class SettingForm_Insurer : Form
    {
        DataTable dtInsurer = new DataTable();
        Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand();
        Npgsql.NpgsqlDataAdapter da = new Npgsql.NpgsqlDataAdapter();
        Npgsql.NpgsqlConnection cn = new Npgsql.NpgsqlConnection();
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        int dgvCurrentRowIndex = 0;

        public SettingForm_Insurer()
        {
            InitializeComponent();
            disp();

        }


        /// <summary>
        /// 表示
        /// </summary>
        private void disp()
        {


            //jyuseiのDBだけはsettings.xmlから取得

            //20201118154544 furukawa st ////////////////////////
            //awsのJyuseiDBの場合の接続文字列作成
            
            if (Settings.strDBserver.Contains("aws"))
            {
                cn.ConnectionString =
                    $"Server={Settings.strDBserver};Port={Settings.intPort};Database=jyusei;User Id=postgres;" +
                    $"Password=medibrain1128;sslmode=require;CommandTimeout=100;";
            }
            else
            {
                cn.ConnectionString =
                    $"Server={Settings.strDBserver};Port={Settings.intPort};Database=jyusei;User Id=postgres;" +
                    $"Password=pass;CommandTimeout=100;";
            }

                        //cn.ConnectionString =
                        //    $"Server={Settings.strDBserver};Port={Settings.intPort};Database=jyusei;User Id=postgres;" +
                        //    $"Password=pass;CommandTimeout=100;";
            //20201118154544 furukawa ed ////////////////////////

            cn.Open();
            cmd.Connection = cn;

            lblserver.Text = $"Server={Settings.strDBserver};Port={Settings.intPort};Database=jyusei;";

            sb.Remove(0, sb.ToString().Length);

            sb.AppendLine(" select ");
            sb.AppendLine(" insurerid		保険者用ID,      ");
            sb.AppendLine(" insurername		保険者名,        ");
            sb.AppendLine(" dbname			DB名,            ");
            sb.AppendLine(" scanorder		ｽｷｬﾝｵｰﾀﾞｰ,       ");
            sb.AppendLine(" enabled			メホール上取扱,  ");
            sb.AppendLine(" insnumber		保険者番号,      ");

            sb.AppendLine(" formalname		保険者正式名称,    ");
            //sb.AppendLine(" formalname		データ格納先,    ");

            sb.AppendLine(" insurertype		保険者タイプ,    ");
            sb.AppendLine(" viewindex		リスト表示順,    ");
            sb.AppendLine(" outputpath		データ出力先,    ");
            sb.AppendLine(" needverify		ベリファイ有無,  ");
            sb.AppendLine(" dbserver		DBサーバ,        ");
            sb.AppendLine(" dbport			DBポート,        ");
            sb.AppendLine(" imagefolderpath	画像フォルダ,     ");

            //20200911153528 furukawa st ////////////////////////
            //insurerテーブルにdbuser、dbpass追加

            sb.AppendLine(" dbuser	DBユーザ,     ");
            sb.AppendLine(" dbpass	DBパス     ");
            //20200911153528 furukawa ed ////////////////////////


            sb.AppendLine(" from                         ");
            sb.AppendLine(" insurer                      ");

            sb.AppendLine(" order by insurerid           ");

            cmd.CommandText = sb.ToString();



            da.SelectCommand = cmd;
            da.Fill(dtInsurer);
            dgv.DataSource = dtInsurer;
            dgv.Columns["保険者用ID"].Width = 50;
            dgv.Columns["保険者名"].Width = 150;
            dgv.Columns["DB名"].Width = 150;
            dgv.Columns["ｽｷｬﾝｵｰﾀﾞｰ"].Width = 50;
            dgv.Columns["メホール上取扱"].Width = 50;
            dgv.Columns["保険者番号"].Width = 80;

            dgv.Columns["保険者正式名称"].Width = 150;
            //dgv.Columns["データ格納先"].Width = 150;

            dgv.Columns["保険者タイプ"].Width = 50;
            dgv.Columns["リスト表示順"].Width = 50;
            dgv.Columns["データ出力先"].Width = 300;
            dgv.Columns["ベリファイ有無"].Width = 50;
            dgv.Columns["DBサーバ"].Width = 100;
            dgv.Columns["DBポート"].Width = 50;
            dgv.Columns["画像フォルダ"].Width = 150;

            //20200911153615 furukawa st ////////////////////////
            //insurerテーブルにdbuser、dbpass追加

            dgv.Columns["DBユーザ"].Width = 100;
            dgv.Columns["DBパス"].Width = 100;
            //20200911153615 furukawa ed ////////////////////////


            dgv.Columns["保険者用ID"].ReadOnly = true;
            dgv.Columns["保険者名"].ReadOnly = true;
            dgv.Columns["DB名"].ReadOnly = true;
            dgv.Columns["ｽｷｬﾝｵｰﾀﾞｰ"].ReadOnly = false;
            dgv.Columns["メホール上取扱"].ReadOnly = false;
            dgv.Columns["保険者番号"].ReadOnly = true;
            dgv.Columns["保険者正式名称"].ReadOnly = true;
            dgv.Columns["保険者タイプ"].ReadOnly = true;
            dgv.Columns["リスト表示順"].ReadOnly = false;
            dgv.Columns["データ出力先"].ReadOnly = false;
            dgv.Columns["ベリファイ有無"].ReadOnly = true;
            dgv.Columns["DBサーバ"].ReadOnly = false;
            dgv.Columns["DBポート"].ReadOnly = false;
            dgv.Columns["画像フォルダ"].ReadOnly = false;

            //20200911153631 furukawa st ////////////////////////
            //insurerテーブルにdbuser、dbpass追加

            dgv.Columns["DBユーザ"].ReadOnly = false;
            dgv.Columns["DBパス"].ReadOnly = false;
            //20200911153631 furukawa ed ////////////////////////

            //20200917115304 furukawa st ////////////////////////
            //削除はメホールからだけにする

            //DataGridViewButtonColumn bc = new DataGridViewButtonColumn();
            //bc.Text = "削除";
            //bc.Width = 60;
            //bc.Name = "削除";
            //bc.UseColumnTextForButtonValue = true;
            //bc.Visible = false;

            //if (!dgv.Columns.Contains("削除")) dgv.Columns.Insert(0, bc);
            //20200917115304 furukawa ed ////////////////////////


            cmbDup.Items.Clear();
            foreach (DataGridViewColumn c in dgv.Columns)
            {
                cmbDup.Items.Add(c.Name);
            }


            dgv.FirstDisplayedScrollingRowIndex = dgvCurrentRowIndex;
            dgv.Columns["保険者名"].Frozen = true;
        }

        /// <summary>
        /// 登録ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonOK_Click(object sender, EventArgs e)
        {

            upd();

        }


        private void upd()
        {
            int col = 0;
            List<string> lstsql = new List<string>();

            foreach (DataRow dr in dtInsurer.Rows)
            {

                if (dr.RowState == DataRowState.Added)
                {
                    col = 0;

                    sb.Remove(0, sb.ToString().Length);
                    sb.AppendLine(" insert into insurer values  (");

                    //20200911153715 furukawa st ////////////////////////
                    //insurerテーブルにdbuser、dbpass追加

                    sb.AppendFormat("'{0}',	'{1}',	'{2}',	'{3}',	'{4}',	'{5}',	'{6}',	'{7}',	'{8}',	'{9}',	'{10}',	" +
                        "'{11}',　'{12}',	'{13}',	'{14}',	'{15}' ",
                         dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(),
                         dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(),
                         dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(),
                         dr[col++].ToString());


                    //sb.AppendFormat("'{0}',	'{1}',	'{2}',	'{3}',	'{4}',	'{5}',	'{6}',	'{7}',	'{8}',	'{9}',	'{10}',	'{11}',	'{12}',	'{13}' ",
                    //    dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(),
                    //    dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(),
                    //    dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString(), dr[col++].ToString());

                    //20200911153715 furukawa ed ////////////////////////

                    sb.AppendLine(");");

                    lstsql.Add(sb.ToString());
                }
                else if (dr.RowState == DataRowState.Modified)
                {
                    col = 1;

                    sb.Remove(0, sb.ToString().Length);
                    sb.AppendLine(" update insurer set ");

                    sb.AppendFormat("	insurername	        ='{0}',", dr[col++].ToString());
                    sb.AppendFormat("	dbname		        ='{0}',", dr[col++].ToString());
                    sb.AppendFormat("	scanorder	        ='{0}',", dr[col++].ToString());
                    sb.AppendFormat("	enabled		        ='{0}',", dr[col++].ToString());
                    sb.AppendFormat("	insnumber	        ='{0}',", dr[col++].ToString());
                    sb.AppendFormat("	formalname	        ='{0}',", dr[col++].ToString());
                    sb.AppendFormat("	insurertype	        ='{0}',", dr[col++].ToString());
                    sb.AppendFormat("	viewindex	        ='{0}',", dr[col++].ToString());
                    sb.AppendFormat("	outputpath	        ='{0}',", dr[col++].ToString());
                    sb.AppendFormat("	needverify	        ='{0}',", dr[col++].ToString());
                    sb.AppendFormat("	dbserver	        ='{0}',", dr[col++].ToString());
                    sb.AppendFormat("	dbport		        ='{0}',", dr[col++].ToString());
                    sb.AppendFormat("	imagefolderpath		='{0}',", dr[col++].ToString());

                    //20200911153833 furukawa st ////////////////////////
                    //insurerテーブルにdbuser、dbpass追加

                    sb.AppendFormat("	dbuser		        ='{0}',", dr[col++].ToString());
                    sb.AppendFormat("	dbpass		        ='{0}' ", dr[col++].ToString());
                    //20200911153833 furukawa ed ////////////////////////


                    sb.AppendFormat("	where insurerid		='{0}';", dr[0].ToString());

                    lstsql.Add(sb.ToString());
                }


            }

            if (lstsql.Count == 0) return;

            //cmd.CommandText = sb.ToString();
            Npgsql.NpgsqlTransaction tran;
            tran = cn.BeginTransaction();


            try
            {

                foreach (string s in lstsql)
                {
                    cmd.CommandText = s;
                    cmd.ExecuteNonQuery();
                }
                tran.Commit();
                MessageBox.Show("更新した");
            }
            catch (Exception ex)
            {
                tran.Rollback();
                MessageBox.Show(ex.Message);

            }
            finally
            {
                dtInsurer.Clear();

                cn.Close();
                disp();
            }


        }

        /// <summary>
        /// id取得
        /// </summary>
        /// <returns></returns>
        private int getMaxID()
        {
            int ret = 0;

            sb.Remove(0, sb.ToString().Length);

            sb.AppendLine("select max(insurerid) id from insurer");
            cmd.CommandText = sb.ToString();
            int.TryParse(cmd.ExecuteScalar().ToString(), out ret);

            return ++ret;


        }

        /// <summary>
        /// 複製ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btncopy_Click(object sender, EventArgs e)
        {
            if (dgv.CurrentRow.Index < 0) return;

            DataGridViewRow dgvr = new DataGridViewRow();

            dgvr = dgv.Rows[dgv.CurrentRow.Index];
            DataRow dr;

            dr = dtInsurer.NewRow();

            for (int r = 1; r < dgvr.Cells.Count; r++)
            {
                if (r == 1)
                {
                    //最大のID取得
                    dr[r - 1] = getMaxID();
                }
                else
                {
                    dr[r - 1] = dgvr.Cells[r].Value.ToString();
                }
            }

            dtInsurer.Rows.Add(dr);
            upd();
        }

        /// <summary>
        /// 削除ボタン列
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex != 0) return;

            //セルを触ったら現在行を控える
            dgvCurrentRowIndex = dgv.CurrentRow.Index;

            string id = dgv.Rows[dgv.CurrentRow.Index].Cells[1].Value.ToString();

            //20201119091312 furukawa st ////////////////////////
            //メホールのJyusei.Insurerテーブルなので、削除するとmejorに影響が出るためやらせない

            //if (MessageBox.Show($"ID{id}を削除します。よろしいですか？",
            //    Application.ProductName, MessageBoxButtons.YesNo,
            //    MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            //{


            //    sb.Remove(0, sb.ToString().Length);
            //    sb.AppendLine(" delete from insurer where insurerid=");
            //    sb.AppendFormat("'{0}'", id);


            //    cmd.CommandText = sb.ToString();
            //    try
            //    {
            //        cmd.ExecuteNonQuery();
            //        MessageBox.Show("削除した");
            //    }
            //    catch (Exception ex)
            //    {
            //        MessageBox.Show(ex.Message);
            //    }
            //    finally
            //    {
            //        dtInsurer.Clear();

            //        cn.Close();
            //        disp();
            //    }
            //}
            //20201119091312 furukawa ed ////////////////////////
        }

        private void SettingForm_Insurer_Shown(object sender, EventArgs e)
        {
            dgv.FirstDisplayedScrollingRowIndex = dgvCurrentRowIndex;
        }

        /// <summary>
        /// 重複チェックボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnChkDup_Click(object sender, EventArgs e)
        {
            string fld = "ｽｷｬﾝｵｰﾀﾞｰ";//デフォルトだけ決めとく
            fld = cmbDup.Text;

            dgv.AlternatingRowsDefaultCellStyle.BackColor = Color.LightGray;

            foreach (DataGridViewRow r in dgv.Rows)
            {
                var tmp = r.Cells[fld].Value.ToString();
                int cnt = 0;

                foreach (DataGridViewRow r2 in dgv.Rows)
                {
                    if (tmp == r2.Cells[fld].Value.ToString())
                    {

                        cnt++;
                        if (cnt > 1)
                        {

                            r.DefaultCellStyle.BackColor = Color.Coral;
                            r2.DefaultCellStyle.BackColor = Color.Coral;
                        }
                        else
                        {
                            r.DefaultCellStyle = null;
                            dgv.AlternatingRowsDefaultCellStyle.BackColor = Color.LightGray;
                        }

                    }
                    else
                    {
                        dgv.AlternatingRowsDefaultCellStyle.BackColor = Color.LightGray;
                    }

                }

            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
