﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BackgroundOCR
{
    public partial class WaitForm : Form
    {
        public bool Cancel = false;

        public WaitForm()
        {
            InitializeComponent();
            vSetAct = new Action<int>(v => progressBar1.Value = v);
        }

        public string Title
        {
            get { return this.Text; }
            set { this.Text = value; }
        }

        public string LabelText
        {
            get { return label1.Text; }
            set { label1.Text = value; }
        }

        public bool CancelButtonEnabled
        {
            get { return button1.Enabled; }
            set { button1.Enabled = value; }
        }

        public int Max
        {
            get { return progressBar1.Maximum; }
            set { progressBar1.Maximum = value; }
        }

        public int Min
        {
            get { return progressBar1.Minimum; }
            set { progressBar1.Minimum = value; }
        }

        public int Value
        {
            get { return progressBar1.Value; }
            set { progressBar1.Value = value; }
        }

        Action<int> vSetAct;
        public int InvokeValue
        {
            get { return progressBar1.Value; }
            set { this.Invoke(vSetAct, value); }
        }

        public ProgressBarStyle BarStyle
        {
            get { return progressBar1.Style; }
            set { progressBar1.Style = value; }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (button1.Text == "キャンセル")
                Cancel = true;
            else
                this.Close();
        }

        public void InvokeCloseDispose()
        {
            Invoke(new Action(() =>
                {
                    this.Close();
                    this.Dispose();
                }));
        }

        public void LogPrint(string str)
        {
            Invoke(new Action(() =>
            {
                this.textBox1.AppendText(str + "\r\n");
            }));
        }
    }
}
