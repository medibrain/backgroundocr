﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackgroundOCR
{
    class CsvImporter
    {
        public static List<string[]> Import(string fileName)
        {
            //CSVファイル名
            string csvFileName = fileName;   //"C:\\test.csv";
            string csvFolderName = System.IO.Path.GetDirectoryName(csvFileName);
            //Shift JISで読み込む
            using (var tfp = new Microsoft.VisualBasic.FileIO.TextFieldParser(
                    csvFileName,
                    System.Text.Encoding.GetEncoding(932)))
            {
                //フィールドが文字で区切られているとする。デフォルトでDelimitedなので、必要なし
                tfp.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited;
                //区切り文字を,とする
                tfp.Delimiters = new string[] { "," };
                //フィールドを"で囲み、改行文字、区切り文字を含めることができるか
                //デフォルトでtrueなので、必要なし
                tfp.HasFieldsEnclosedInQuotes = true;
                //フィールドの前後からスペースを削除する。デフォルトでtrueなので、必要なし
                tfp.TrimWhiteSpace = true;
                var l = new List<string[]>();

                while (!tfp.EndOfData)
                {
                    //フィールドを読み込む
                    try
                    {
                        l.Add(tfp.ReadFields());
                    }
                    catch (Microsoft.VisualBasic.FileIO.MalformedLineException ex)
                    {
                        var line = tfp.ErrorLine;
                        Log.ErrorWrite(ex);
                        Log.InfoWrite(line);
                    }
                }

                return l;

                //todo:エラー時の返り値が無い
            }
        }
    }
}
