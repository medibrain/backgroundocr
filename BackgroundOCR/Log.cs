﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace BackgroundOCR
{
    public static class Log
    {
        static string erdir = System.Windows.Forms.Application.StartupPath +
            "\\" + "\\Error\\";
        
        static string infodir = System.Windows.Forms.Application.StartupPath +
            "\\" + "\\Info\\";

        static Log()
        {
            var func = new Func<string, bool>((p) =>
                {
                    if (System.IO.Directory.Exists(p)) return true;
                    try
                    {
                        System.IO.Directory.CreateDirectory(p);
                        return true;
                    }
                    catch
                    {
                        System.Windows.Forms.MessageBox.Show("ログディレクトリの作成に失敗しました。" +
                            "受信は中断されています。設定を見直し、ソフトを起動しなおしてください。");
                        return false;
                    }
                });

            if (!func(erdir)) return;
            if (!func(infodir)) return;
        }


        /// <summary>
        /// エラーメッセージログを書き込みます
        /// </summary>
        /// <param name="logText"></param>
        /// <returns></returns>
        public static void ErrorWrite(string logText)
        {
            string fileName = erdir + DateTime.Today.ToString("yyMMddHHmmss") + ".log";
            for (int i = 0; i < 100; i++)
            {
                try
                {
                    using (var sw = new StreamWriter(fileName, true, Encoding.GetEncoding("Shift_JIS")))
                    {
                        sw.WriteLine(DateTime.Now.ToString() + "," + logText);
                    }
                }
                catch
                {
                    continue;
                }
                break;
            }
        }

        /// <summary>
        /// エラーログを書き込みます
        /// </summary>
        /// <param name="ex"></param>
        public static void ErrorWrite(Exception ex)
        {
            for (int i = 0; i < 100; i++)
            {
                try
                {
                    string fileName = erdir + "Error" + DateTime.Now.ToString("yyMMddHHmmss") + ".log";
                    if (System.IO.File.Exists(fileName))
                    {
                        System.Threading.Thread.Sleep(10);
                        continue;
                    }

                    using (var sw = new System.IO.StreamWriter(fileName))
                    {
                        sw.Write(ex.ToString());
                    }
                }
                catch
                {
                    continue;
                }
                break;
            }
        }

        /// <summary>
        /// エラーログを書き込みます
        /// </summary>
        /// <param name="ex"></param>
        public static void ErrorWriteWithMsg(Exception ex)
        {
            for (int i = 0; i < 100; i++)
            {
                try
                {
                    string fileName = erdir + "Error" + DateTime.Now.ToString("yyMMddHHmmss") + ".log";
                    if (System.IO.File.Exists(fileName))
                    {
                        System.Threading.Thread.Sleep(10);
                        continue;
                    }

                    using (var sw = new System.IO.StreamWriter(fileName))
                    {
                        sw.Write(ex.ToString());
                    }
                }
                catch
                {
                    continue;
                }
                break;
            }

            System.Windows.Forms.MessageBox.Show("エラーが発生しました\r\n\r\n" + ex.Message,
                "エラー", System.Windows.Forms.MessageBoxButtons.OK,
                System.Windows.Forms.MessageBoxIcon.Error);
        }

        public static void InfoWrite(string str)
        {
            DateTime dt = DateTime.Now;
            var fileName = infodir + dt.ToString("yyyyMMdd") + ".log";

            try
            {
                using (var sw = new System.IO.StreamWriter(fileName, true, Encoding.GetEncoding("Shift_JIS")))
                {
                    sw.WriteLine(dt.ToString("HH:mm:ss:fff\t") + str + "\r\n");
                }
            }
            catch
            {
                Log.ErrorWrite("インフォログ書き込み失敗 :" + str);
            }
        }

        public static void ocrLogWrite(string dbname, int scanid)
        {
            DateTime dt = DateTime.Now;
            var fileName = infodir + "ocrLog_" + dt.ToString("yyyyMMdd") + ".log";

            try
            {
                using (var sw = new System.IO.StreamWriter(fileName, true, Encoding.GetEncoding("Shift_JIS")))
                {
                    sw.WriteLine(dt.ToString("HH:mm:ss") + " => " + dbname + ":" + scanid.ToString() + " => 正常終了\r\n");
                }
            }
            catch
            {
                Log.ErrorWrite("OCRログ書き込み失敗");
            }
        }
    }
}
