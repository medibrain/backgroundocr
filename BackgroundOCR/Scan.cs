﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NpgsqlTypes;
using System.Xml;

namespace BackgroundOCR
{
    //20210505163929 furukawa st ////////////////////////
    //BackgroundOCRを複数動かす場合、OCRをするscanidを被らさないための処置
    //OCR中=25追加
    public enum SCAN_STATUS { NULL = 0, SCANING = 1, 取込済み = 2, OCR済み = 3, OCR中=25}
    //20210505163929 furukawa ed ////////////////////////


    public class Scan
    {
        public int sid { get; private set; }
        public DateTime scanDate { get; set; }
        public int cyear { get; set; }
        public int cmonth { get; set; }
        public string note1 { get; set; }
        public string note2 { get; set; }
        public SCAN_STATUS status { get; set; }

        public bool Update()
        {
            using (var cmd = DB.CreateCmd("UPDATE scan SET " +
                "scandate=:sdate, cyear=:cy, cmonth=:cm, " +
                "note1=:n1, note2=:n2, status=:st " +
                "WHERE sid=:sid"))
            {
                cmd.Parameters.Add("sdate", NpgsqlDbType.Date).Value = scanDate;
                cmd.Parameters.Add("cy", NpgsqlDbType.Integer).Value = this.cyear;
                cmd.Parameters.Add("cm", NpgsqlDbType.Integer).Value = this.cmonth;
                cmd.Parameters.Add("n1", NpgsqlDbType.Text).Value = this.note1;
                cmd.Parameters.Add("n2", NpgsqlDbType.Text).Value = this.note2;
                cmd.Parameters.Add("st", NpgsqlDbType.Integer).Value = (int)this.status;
                cmd.Parameters.Add("sid", NpgsqlDbType.Integer).Value = this.sid;

                try
                {
                    return cmd.TryExecuteNonQuery();
                }
                catch
                {
                    return false;//このエラーログは呼び出し元で記録する
                }
            }
        }



        /// <summary>
        /// //ステータスでスキャンIDを抽出します
        /// </summary>
        /// <param name="status">ステータス</param>        
        /// <returns></returns>
        public static List<Scan> GetScanList(SCAN_STATUS status)
        {
          
            using (var cmd = DB.CreateCmd("SELECT " +
                "sid, scandate, cyear, cmonth, note1, note2, status " +
                "FROM scan WHERE status=:st ORDER BY sid"))
            {


                cmd.Parameters.Add("st", NpgsqlDbType.Integer).Value = (int)status;



                //20201118180832 furukawa st ////////////////////////
                //取れない場合null
                
                List<Object[]> res;
                try
                {
                    res = cmd.TryExecuteReaderList();
                }
                catch
                {
                    cmd.Dispose();
                    return null;
                }

                            //var res = cmd.TryExecuteReaderList();
                //20201118180832 furukawa ed ////////////////////////



                if (res == null) return null;

                var l = new List<Scan>();

                foreach (var item in res)
                {
                    var s = new Scan();

                    s.sid = (int)item[0];
                    s.scanDate = (DateTime)item[1];
                    s.cyear = (int)item[2];
                    s.cmonth = (int)item[3];
                    s.note1 = (string)item[4];
                    s.note2 = (string)item[5];
                    s.status = (SCAN_STATUS)(int)item[6];
                    l.Add(s);
                }
                cmd.Dispose();
                return l;
            }
        }


        /// <summary>
        /// OCRを実行します。実行に失敗するかOCR対象がなかった場合、falseが返ります。
        /// </summary>
        /// <param name="ins"></param>
        /// <param name="f"></param>
        /// <param name="lstBlack">画像パスがなかったScanIDのリスト</param>
        /// <returns></returns>
        public static bool DoOCR(Insurer ins, FormMain f)
            //public static bool DoOCR(Insurer ins, FormMain f, List<int> lstBlack)
        {
           
            try
            {
                //接続先データベースを保険者に合わせる
                DB.SetDBName(ins.dbName);
                


                //画像登録されたScanIDリストを取得
                var l = Scan.GetScanList(SCAN_STATUS.取込済み);

                if (l == null || l.Count == 0)
                {
                    //20190822153232 furukawa st ////////////////////////
                    //取込候補がない場合メッセージ
                    
                    f.WriteLog(ins.InsurerName + "取込候補なし" );
                    //20190822153232 furukawa ed ////////////////////////
                    
                    return false;
                }

                var s = l[0];
                f.TimeCountStart(ins, s);
                f.WriteLog(ins.InsurerName + "  ScanID:" + s.sid + " のOCRを開始します");
                

                //20200210150242 furukawa st ////////////////////////
                //スキャン画像コピー先パスを画面に出す                
                bool success = doScanOcr(ins, s,f);
                //bool success = doScanOcr(ins, s);
                //20200210150242 furukawa ed ////////////////////////

                if (success) f.WriteLog(ins.InsurerName + "  ScanID:" + s.sid + " のOCRが終了しました");
                else f.WriteLog(ins.InsurerName + "  ScanID:" + s.sid + " のOCRに失敗しました");
                   
                
                f.TimeCountStop();
                return success;
            }
            finally
            {
                //接続先データベースを元に戻す
                DB.SetDBName("jyusei");
            }
        }

        //OCR処理シーケンス

        //20200210150908 furukawa st ////////////////////////
        //スキャン画像コピー先パスを画面に出すため引数追加        
        public static bool doScanOcr(Insurer ins, Scan scan,FormMain f)
            //public static bool doScanOcr(Insurer ins, Scan scan)
        //20200210150908 furukawa ed ////////////////////////
        {

            const int OsakaKoiki =4;


            //20201119161053 furukawa st ////////////////////////
            //jyusei.insurerテーブルから取得
            
            var ip =  ins.imagefolderpath;

                    //20200210144620 furukawa st ////////////////////////
                    //パスもsettings.xmlに共通化したのでそっちからとる
                    //var ip = "\\\\" + Settings.strImageFolder;
                            //var ip = BackgroundOCR.Properties.Settings.Default.ImageFolder;
                    //20200210144620 furukawa ed ////////////////////////


            //20201119161053 furukawa ed ////////////////////////

            ip += "\\" + DB.GetDBName();
            var ocrPath = ip + "\\" + scan.sid.ToString();
            
            f.WriteLog(ocrPath);

            string formatType = scan.note2;
            //20210505160432 furukawa st ////////////////////////
            //大阪広域が300DPIになってた
            //if (ins.InsurerID == OsakaKoiki && scan.note2 == string.Empty) formatType = "JyuOsaka200";
            //20210505160432 furukawa ed ////////////////////////

            //20220111113120 furukawa st ////////////////////////
            //コロナを含む場合はcovid19フォーマットを読む            
            if (ins.InsurerName.Contains("コロナ")) formatType = "covid19";
            //20220111113120 furukawa ed ////////////////////////



            //20200210162555 furukawa st ////////////////////////
            //メホール本体画像取込したパスとOCR参照先が違う場合がある

            if (!System.IO.Directory.Exists(ocrPath))
            {
                f.WriteLog($"{ocrPath}は存在しません。スキップします");
                return false;
            }

            //20200210162555 furukawa ed ////////////////////////
            

            //設定ファイルを編集する
            var settingFile = frtxEdit(ocrPath, formatType);

            if (settingFile == "-1")
            {
                Log.ErrorWrite("ScanID= " + scan.sid + " のOCR設定ファイル編集に失敗しました。");
                return false;
            }

            //もし画像フォルダに既にResult.csvが存在する場合、名前を変更
            if (System.IO.File.Exists(ocrPath + "\\OCR_Result.csv"))
            {
                try
                {
                    System.IO.File.Move(ocrPath + "\\OCR_Result.csv",
                        ocrPath + "\\OCR_Result" + DateTime.Now.ToString("yyMMddHHmmss") + ".csv");
                }
                catch
                {
                    Log.ErrorWrite("ScanID= " + scan.sid + " のOCR結果CSVファイルの重複を解決できませんでした。");
                    return false;
                }
            }


            //20210505164322 furukawa st ////////////////////////
            //BackgroundOCRを複数動かす場合、OCRをするscanidを被らさないための処置。OCRを通すのは取込済みなので、別のステータスであればいい            
            scan.status = SCAN_STATUS.OCR中;
            if (!scan.Update())
            {
                Log.ErrorWrite($"ScanID= {scan.sid} のステータス {SCAN_STATUS.OCR中} への更新に失敗しました。");
                return false;
            }

            //20210505164322 furukawa ed ////////////////////////



            //OCRコマンドを生成

            //20200210144816 furukawa st ////////////////////////
            //OCRプログラムパスもsettings.xmlに共通化したのでそっちからとる          
            var command = Settings.strOCRProgram;
                    //var command = Properties.Settings.Default.OCRApp;
            //20200210144816 furukawa ed ////////////////////////


            command += " /P=\"" + settingFile + "\" "; //設定ファイルの指定
            command += "/E";

            //OCRコマンドを投げる
            using (var p = new System.Diagnostics.Process())
            {
                //20200210144949 furukawa st ////////////////////////
                //OCRプログラムパスもsettings.xmlに共通化したのでそっちからとる
                
                p.StartInfo.FileName = Settings.strOCRProgram;
                        //p.StartInfo.FileName = Properties.Settings.Default.OCRApp;
                //20200210144949 furukawa ed ////////////////////////


                p.StartInfo.Arguments = " /P=\"" + settingFile + "\" /E";
                p.Start();
                p.WaitForExit();
            }

            //csvファイルの読込
            var lines = new List<string>();
            using (var sr = new System.IO.StreamReader(ocrPath + "\\OCR_Result.csv", Encoding.GetEncoding("Shift_JIS")))
            {
                while (sr.Peek() > 0) lines.Add(sr.ReadLine());
            }

            for (int i = 0; i < lines.Count; i++)
            {
                if (!SetOcrData(lines[i]))
                {
                    Log.ErrorWrite(
                        $"ScanID={scan.sid}  {i + 1}行目のOCR結果CSVによるApplicationレコード更新に失敗しました。");
                }
            }

            //OCR処理完了フラグを立てる
            scan.status = SCAN_STATUS.OCR済み;

            if (!scan.Update())
            {
                Log.ErrorWrite("ScanID= " + scan.sid +" のステータス「OCR」への更新に失敗しました。");
                return false;
            }

            try
            {
                var sgl = ScanGroup.GetScanGroupListSID(scan.sid);
                foreach (var sg in sgl)
                {
                    //ScanGoupのStatusが"OCR済み"以下であれば、Statusを"OCR済み"に更新
                    if ((int)sg.Status <= 2)
                    {
                        sg.Status = groupStatus.OCR済み;
                        ScanGroup.Update(sg);
                    }
                }
            }
            catch(Exception ex)
            {
                Log.ErrorWrite(ex);
                Log.ErrorWrite("ScanID= " + scan.sid + " に含まれるScanGroupのステータス更新に失敗しました。");
                return false;
            }

            Log.ocrLogWrite(ins.dbName, scan.sid);

            return true;
        }


        /// <summary>
        /// 設定ファイルを作成します
        /// </summary>
        /// <param name="imageFolderFullPath"></param>
        /// <returns>設定ファイルのフルパス</returns>
        public static string frtxEdit(string imageFolderFullPath, string ftype)
        {
            var appPath = System.Windows.Forms.Application.StartupPath +"\\frtx";
            //var appPath = System.Windows.Forms.Application.StartupPath;
            string fn;

            if (ftype == "7")
            {
                fn = appPath + "\\HariKyu.frtx";
            }
            else if (ftype == "8")
            {
                fn = appPath + "\\Anma.frtx";
            }
            else if(ftype == "JyuOsaka200")
            {
                fn = appPath + "\\JyuOsaka200.frtx";
            }
            
            //20220111113247 furukawa st ////////////////////////
            //コロナを含む場合はcovid19フォーマットを読む            
            else if (ftype == "covid19")
            {
                fn = appPath + "\\covid19.frtx";
            }
            //20220111113247 furukawa ed ////////////////////////

            else
            {
                fn = appPath + "\\Jyu.frtx";
            }
                
            var nfn = appPath + "\\Current.frtx";

            if (System.IO.File.Exists(nfn))
            {
                System.IO.File.Delete(nfn);
            }
            System.IO.File.Copy(fn, nfn);

            try
            {
                var doc = new XmlDocument();
                doc.Load(nfn);

                //対象フォルダ指定
                var imagenode = doc.SelectSingleNode("ruleSetting").SelectSingleNode("ruleInfo").SelectSingleNode("画像の入力");
                imagenode.SelectSingleNode("m_strInputFolder").InnerText = imageFolderFullPath;

                //CSV出力先
                var savenode = doc.SelectSingleNode("ruleSetting").SelectSingleNode("ruleInfo").SelectSingleNode("認識結果");
                savenode.SelectSingleNode("m_strRecogSaveOutputFolder").InnerText = imageFolderFullPath;
                savenode.SelectSingleNode("m_strRecogSaveFileName").InnerText = "OCR_Result";

                doc.Save(nfn);
                return nfn;
            }
            catch(Exception ex)
            {
                Log.ErrorWrite(ex.Message);
                return "-1";
            }
        }


        /// <summary>
        /// Applicationレコードに、OCR結果CSVデータを付加します
        /// </summary>
        /// <param name="strs"></param>
        /// <returns></returns>
        public static bool SetCSVData(string[] strs, string str)
        {
            //todo:拡張子.tif固定で良いか？
            var oldimageFileName = strs[1] + ".tif";

            using (var cmd = DB.CreateCmd("UPDATE application " +
                "SET aall=:al, ocrdata=:ocrdata WHERE aimagefile=:af"))
            {
                cmd.Parameters.Add("al", NpgsqlDbType.Text | NpgsqlDbType.Array).Value = strs;
                cmd.Parameters.Add("ocrdata", NpgsqlDbType.Text).Value = str;
                cmd.Parameters.Add("af", NpgsqlDbType.Text).Value = oldimageFileName;

                try
                {
                    return cmd.TryExecuteNonQuery();
                }
                catch
                {
                    return false;//このエラーログは呼び出し元で記録する
                }
            }
        }

        /// <summary>
        /// Applicationレコードに、OCR結果CSVデータを付加します
        /// </summary>
        /// <param name="strs"></param>
        /// <returns></returns>
        public static bool SetOcrData(string str)
        {
            var strs = str.Split(',');
            if (strs.Length < 3) return false;

            var oldimageFileName = strs[1] + ".tif";

            using (var cmd = DB.CreateCmd("UPDATE application " +
                "SET ocrdata=:ocrdata WHERE aimagefile=:aimagefile"))
            {
                cmd.Parameters.Add("ocrdata", NpgsqlDbType.Text).Value = str;
                cmd.Parameters.Add("aimagefile", NpgsqlDbType.Text).Value = oldimageFileName;

                try
                {
                    return cmd.TryExecuteNonQuery();
                }
                catch(Exception ex)
                {
                    Log.ErrorWrite(ex);
                    return false;//このエラーログは呼び出し元で記録する
                }
            }
        }

    }
}
